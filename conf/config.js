const env = process.env.NODE_ENV || "development";

// Development Configuration
const development = {
    commentsClient: "https://jsonplaceholder.typicode.com/"
};

const config = {
    development
};

module.exports = config[env];
