# Comments Service
This service will fetch 200 posts and their corresponding comments adding comments size the response JSON as well.
  
![alt text](images/basic-flow.png)

## Getting started

1. clone the project on your local machine:

```shell
 https://gitlab.com/msherazy/comments-service.git
```

2. cd to project folder:

```shell
 cd comments-service
```

3. install dependencies:

```shell
 npm install
```

4. serve application on `http://localhost:3000` in your browser:

```shell
 npm start
```
This application contains ```docker-compose.yml``` and ```Dockerfile``` which can be use to service application.

```docker-compose.yml```
```sh
# docker-compose.yml
version: "3.9"
services:
  comments-service:
    build:
      context: .
    environment:
      NODE_ENV: development
    ports:
      - '8080:3000'
```

```Dockerfile```
```sh
# Dockerfile
FROM node:16.17.0

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

# Server port
EXPOSE 3000

# Starting applications
CMD [ "npm", "start" ]
```

### Local Environment with docker
When developing, many times we want to test out app in actual ```docker container```. If you want you can test your app during ongoing development run ```docker composer up -d``` from root folder of application. You can access application from ``` SERVE:PORT ``` in case of local environment it will be ``` localhost:8080 ```. 

You can stop it as well using ```docker composer down``` to stop service application

### Production Environment
There are multiple ways to serve a docker application on production server and CI/CD always plays great role in situation.
##### CI/CD
Using any CI/CD tools can help using to automate integration and deployment of service. It will help using to test application bugs, its integration and continues deployment on production servers. 
##### Production Infra
Once docker image is ready we can deploy in production, it can be either aws-fargate, eks, aws-lambda or even simple server attached to lb.

Below diagram is to show production ready setup for comments-service
![alt text](images/comments-service.png)
