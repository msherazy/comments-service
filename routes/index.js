var express = require('express');
var axios = require("axios");
var router = express.Router();


router.get('/', async (req, res, next) => {

    try {
        let postsResponse = await axios.get('https://jsonplaceholder.typicode.com/posts');
        let posts = postsResponse.data;
        let postArrayResponse = [];
        const requests = posts.map((post) => {
            post['comments']= [];
            postArrayResponse.push(post);
            return axios.get("https://jsonplaceholder.typicode.com/comments?postId=" + post.id)
        });
        try {
            // wait until all the api calls resolves
            const result = await Promise.all(requests);
            // posts are ready. accumulate all the posts without duplicates
            result.map((item) => {
                if(item.data.length){
                    const postId = item.data[0]['postId'];
                    posts.map((p)=>{
                        if(p.id == postId){
                            item.data.map((i)=>{
                                i['size'] = i['body'].length;
                            })
                            p['comments'] = item.data;
                        }
                    })
                }

            });

        } catch (err) {
            res.status(500).json({error: String(err)});
        }

        res.send({"status": "ok", "data": postArrayResponse});

    } catch (e) {
        console.log(e)
    }
});

module.exports = router;
